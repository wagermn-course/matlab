# 数学实验

## 内容简介
1. MATLAB 概述
2. MATLAB 基本运算
3. MATLAB 绘图
4. MATLAB 程序设计

## 相关链接
* [MATLAB 官方网站](https://www.mathworks.com) 
* [MATLAB 官方帮助文档](https://www.mathworks.com/help/matlab/index.html)
* [Octave 官方网站](http://www.gnu.org/software/octave/)